from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve, diplomacy_read, diplomacy_eval, diplomacy_print

class TestDiplomacy(TestCase):
    #read
    def test_read_1(self):
        s = 'A Mardid Hold\n'
        x,y,z = diplomacy_read(s)
        self.assertEqual(x, 'A')
        self.assertEqual(y, 'Mardid')
        self.assertEqual(z, 'Hold')
       
        
    def test_read_2(self):
        s = 'B Barcelona Move Madrid\n'
        x,y,z = diplomacy_read(s)
        self.assertEqual(x, 'B')
        self.assertEqual(y, 'Barcelona')
        self.assertEqual(z, 'Move Madrid')
        
    def test_read_3(self):
        s = 'E Austin Support A\n'
        x,y,z = diplomacy_read(s)
        self.assertEqual(x, 'E')
        self.assertEqual(y, 'Austin')
        self.assertEqual(z, 'Support A')
    #eval
    def test_eval_1(self):
        s = {'London': ('A', 'Hold'), 'Madrid': ('B', 'Hold')}
        v = diplomacy_eval(s)
        t = {'A': 'London', 'B': 'Madrid'}
        self.assertEqual(v, t)
    # diplomacy_solve
    def test_diplomacy_solve_1(self):
        v =StringIO('A London Hold\nB Madrid Hold\n')
        w = StringIO()
        diplomacy_solve(v,w)
        self.assertEqual(w.getvalue(),'A London\nB Madrid\n')

    def test_diplomacy_solve_2(self):
        v = StringIO('A Madrid Hold\nB Barcelona Move Madrid\n')
        w = StringIO()
        diplomacy_solve(v,w)
        self.assertEqual(w.getvalue(), 'A [Dead]\nB [Dead]\n')
        
    def test_diplomacy_solve_3(self):
        v = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n')
        w = StringIO()
        diplomacy_solve(v,w)
        self.assertEqual(w.getvalue(), 'A [Dead]\nB Madrid\nC London\n')
    def test_print_1(self):
        w = StringIO()
        t = {'A': 'London', 'B': 'Madrid'}
        diplomacy_print(w, t)
        self.assertEqual(w.getvalue(), "A London\nB Madrid\n")

if __name__ == "__main__":
    main()
