from unittest import main, TestCase
from itertools import count

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_solve, diplomacy_print


class MyUnitTests (TestCase) :

#
#   read
#
    def test_read1 (self) :
        st="A Madrid Hold"
        ls= ["A", "Madrid", "Hold"]
        self.assertEqual(ls , diplomacy_read(st))
        
    def test_read2 (self) :
        st="B Paris Move Madrid"
        ls= ["B", "Paris", "Move", "Madrid"]
        self.assertEqual(ls , diplomacy_read(st))
        
    def test_read3 (self) :
        st="C Cupertino Support Austin"
        ls= ["C", "Cupertino", "Support", "Austin"]
        self.assertEqual(ls , diplomacy_read(st))
      
    #
    # eval
    #
    
    def test_eval1 (self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_eval(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_eval2 (self) :
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid")
        w = StringIO()
        diplomacy_eval(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")
        
    def test_eval3 (self) :
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
        w = StringIO()
        diplomacy_eval(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")
    
    def test_eval4 (self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\n")
        w = StringIO()
        diplomacy_eval(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

    def test_eval5 (self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\n")
        w = StringIO()
        diplomacy_eval(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")

    
    def test_eval6(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n")
        w = StringIO()
        diplomacy_eval(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")
    
    def test_eval7(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Hold\nF Cupertino Support E\nG Houston Move Dallas\nH Dallas Move Houston\n")
        w = StringIO()
        diplomacy_eval(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\nE Austin\nF Cupertino\nG Dallas\nH Houston\n")
    
    def test_eval8(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Austin Move London\nE Cupertino Move SanFran\nF SanFran Support E\nG Houston Support H\nH Seattle Support I\nI Detroit Move Houston\n")
        w = StringIO()
        diplomacy_eval(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\nE [dead]\nF [dead]\nG [dead]\nH Seattle\nI Houston\n")
    
    
    
    
    
    #
    #   print
    #
    
    def test_print1 (self) :
        w = StringIO()
        tuplist=[("A","London"), ("B","[dead]"), ("C","Madrid") ]
        diplomacy_print(w,tuplist)
        self.assertEqual(w.getvalue() , "A London\nB [dead]\nC Madrid\n")
        
    def test_print2 (self) :
        w = StringIO()
        tuplist=[("A","Cupertino"), ("B","Austin"), ("C","Russia"), ("D","[dead]")]
        diplomacy_print(w,tuplist)
        self.assertEqual(w.getvalue() , "A Cupertino\nB Austin\nC Russia\nD [dead]\n")
        
    def test_print3 (self) :
        w = StringIO()
        tuplist=[("A","[dead]"), ("B","[dead]"), ("C","[dead]"), ("D","[dead]"),("E","[dead]"),("F","[dead]")]
        diplomacy_print(w,tuplist)
        self.assertEqual(w.getvalue() , "A [dead]\nB [dead]\nC [dead]\nD [dead]\nE [dead]\nF [dead]\n")
        
        
    # 
    #solve
    #
    
    def test_solve1 (self) :
        ll=[["A", "Madrid", "Hold"],["B", "Barcelona", "Move", "Madrid"],["C", "London", "Support", "B"]]                                         
        self.assertEqual(diplomacy_solve(ll) , [("A", "[dead]"), ("B", "Madrid"), ("C", "London")])
        
    def test_solve2 (self) :
        ll = [["A", "Madrid", "Hold"],["B", "Barcelona", "Move", "Madrid"],["C", "London", "Support", "B"], ["D", "Austin", "Move", "London"]]
        self.assertEqual(diplomacy_solve(ll), [("A", "[dead]"), ("B", "[dead]"), ("C", "[dead]"), ("D", "[dead]")])
        
    def test_solve3 (self) :
        ll = [["A", "Madrid", "Hold"],["B", "Barcelona", "Move", "Madrid"],["C", "London", "Move", "Madrid"], ["D", "Paris", "Support", "B"], ["E", "Austin", "Support", "A"]]
        self.assertEqual(diplomacy_solve(ll) , [("A", "[dead]"), ("B", "[dead]"), ("C", "[dead]"), ("D", "Paris"), ("E", "Austin")])
    
    
    
if __name__ == "__main__":
    main()