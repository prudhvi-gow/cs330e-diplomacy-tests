# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_print, diplomacy_solve

# -----------
# Testdiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "A Madrid Hold\n"
        army, loc, action = diplomacy_read(s)
        self.assertEqual(army,  "A")
        self.assertEqual(loc, "Madrid")
        self.assertEqual(action, ("Hold",))

    def test_read_2(self):
        s = "B Barcelona Move Madrid\n"
        army, loc, action = diplomacy_read(s)
        self.assertEqual(army, "B")
        self.assertEqual(loc, "Barcelona")
        self.assertEqual(action, ("Move", "Madrid"))

    def test_read_3(self):
        s = "D Paris Support B\n"
        army, loc, action = diplomacy_read(s)
        self.assertEqual(army, "D")
        self.assertEqual(loc, "Paris")
        self.assertEqual(action, ("Support", "B"))


    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, "A", "Madrid")
        self.assertEqual(w.getvalue(), "A Madrid\n")

    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, "B", "[dead]")
        self.assertEqual(w.getvalue(), "B [dead]\n")

    def test_print_3(self):
        w = StringIO()
        diplomacy_print(w, "Test", "String")
        self.assertEqual(w.getvalue(), "Test String\n")


    # -----
    # solve (uses eval as a helper function)
    # -----

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\n")

    def test_solve_2(self):
        r = StringIO("A Madrid Move Austin\nB Austin Move Dallas\nC Dallas Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Austin\nB Dallas\nC Madrid\n")

    def test_solve_3(self):
        r = StringIO("A Austin Hold\nB Dallas Move Austin\nC Houston Support B\nD Madrid Move Houston\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_solve_4(self):
        r = StringIO("A Austin Hold\nB Dallas Move Austin\nC Houston Support B\nD Madrid Move Houston\nE Rio Support C")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC Houston\nD [dead]\nE Rio\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover"""