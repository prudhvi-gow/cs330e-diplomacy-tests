from io import StringIO
from unittest import main, TestCase
from Diplomacy import diplomacy_solve
from Army import Army
from Location import Location

# -----------
# TestDiplomacy
# -----------

class TestDiplomacy(TestCase):

    def test_solve(self):
        r = StringIO("J Austin Move Ft_Worth")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "J Ft_Worth\n")
    
    def test_solve2(self):
        r = StringIO("J Ft_Worth Hold\nA Austin Move Ft_Worth\nB Houston Support J")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "J Ft_Worth\nA [dead]\nB Houston\n")
    
    def test_solve3(self):
        r = StringIO("A Austin Support B\nB Austin Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\n")

    def test_solve4(self):
        r = StringIO("A Austin Hold\nB Austin Move Ft_Worth")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Austin\nB Ft_Worth\n")
    
    def test_solve5(self):
        r = StringIO("A Austin Hold\nB Austin Hold\nC Ft_Worth Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Austin\nC Ft_Worth\n")

    def test_solve6(self):
        r = StringIO("A Austin Hold\nB Austin Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\n")
    
    def test_locationString(self):
        Location.allLocations = []
        a = Army("J Home Hold")
        self.assertEqual(
            str(a.location), "Home: [J Home]")
        Location.allLocations = []
    
    def test_locationRepr(self):
        Location.allLocations = []
        a = Army("K Home Hold")
        b = Army("L Austin Hold")
        self.assertEqual(
            str(Location.allLocations), "[Home: [K Home], Austin: [L Austin]]")
        Location.allLocations = []

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
"""